# mastodon-snewbot


This simple bot uses Reddit streams to follow subreddits and repost their submissions to Mastodon.


## Configuration

The following confuiguration environment variables are required:

```
MASTODON_ACCESS_TOKEN=<your bot accounts Mastodon access token>
MASTODON_API_BASE_URL=<your Mastodon instance url, including scheme>
MASTODON_VISIBILITY=["public"]<the visibility of posts on Mastodon made by this bot>
MASTODON_ADMIN_USERNAME=["admin"]<the username on Mastodon for the bot to send errors to>
TWITTER_CONSUMER_KEY=<your Twitter consumer key>
TWITTER_CONSUMER_SECRET=<your Twitter consumer secret>
TWITTER_ACCESS_TOKEN=<your Twitter access token>
TWITTER_ACCESS_TOKEN_SECRET=<your Twitter access token secret>
TWITTER_HANDLE=<the twitter handle you want to follow, without the @>
```


## running the bot

Run it in a docker container. All configuration parameters should be set through environment variables:

```
docker run --rm -e MASTODON_ACCESS_TOKEN="asdfljhsdfuisdk" ... registry.gitlab.com/gdunstone/mastodon-twitbot:latest
```

Thats all folks.

Please dont use this for evil.
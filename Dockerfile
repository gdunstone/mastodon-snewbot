FROM alpine

RUN apk add --no-cache py3-pip py3-requests py3-yaml git

RUN pip3 install --no-cache-dir praw

RUN git clone https://github.com/gdunstone/Mastodon.py.git && \
    cd Mastodon.py && \
    python3 setup.py install

ENV MASTODON_API_BASE_URL ""
ENV MASTODON_VISIBILITY "public"
ENV REDDIT_CLIENT_ID ""
ENV REDDIT_CLIENT_SECRET ""

ENV PYTHONUNBUFFERED=1
VOLUME /data

WORKDIR /usr/src/app
COPY . .

CMD ["python3", "run.py"]
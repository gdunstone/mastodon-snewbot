#!/usr/bin/env python3
import praw
import yaml
import os
import time
import mastodon
import requests
import traceback
import random
import string

config_path = os.environ.get("CONFIG", "config.yaml")

config = yaml.load(open(config_path), Loader=yaml.FullLoader)

print("Logging into Reddit")

reddit = praw.Reddit(
    client_id=os.environ["REDDIT_CLIENT_ID"],
    client_secret=os.environ["REDDIT_CLIENT_SECRET"],
    user_agent="PRAW Mastodon subreddit relay")


def get_content_type(url):
    return requests.head(url).headers.get('Content-Type')


def try_remove(fn):
    try:
        os.remove(fn)
    except Exception:
        pass


def get_media(url):

    def rch():
        return random.choice(string.ascii_letters)
    random_prefix = "".join([rch() for _ in range(6)])
    lastchunk = url.split('/')[-1]
    fn = f"{random_prefix}.{lastchunk.split('.')[-1]}"
    print(f"temporarily saving {url} in {fn}...")
    response = requests.get(url)
    if response.status_code == 200:
        with open(fn, 'wb') as f:
            f.write(response.content)
    return fn


for sub_name, conf in config.items():
    masto = mastodon.Mastodon(
        access_token=conf['mastodon_access_token'],
        api_base_url=os.environ.get("MASTODON_API_BASE_URL")
    )
    subreddit = reddit.subreddit(sub_name)
    files = []
    try:
        try:
            url = subreddit.icon_img
            fn = get_media(url)
            files.append(fn)
            cachepath = f"{masto.me().acct}/avatar".encode()
            print(f"@{masto.me().acct}:avatar\t\t->\t{fn}")
            masto.account_update_credentials(avatar=fn)

        except Exception:
            traceback.print_exc()

        try:
            banner_url = subreddit.banner_img
            bfn = get_media(banner_url)
            files.append(bfn)
            cachepath = f"{masto.me().acct}/header".encode()
            print(f"@{masto.me().acct}:header\t\t->\t{bfn}")
            masto.account_update_credentials(header=bfn)
        except Exception:
            traceback.print_exc()

        print(f"@{masto.me().acct}:login\t\t->\tProfile update complete")
    except Exception:
        traceback.print_exc()
    finally:
        for file in files:
            try_remove(file)

# we stream from multiple subreddits
sub = reddit.subreddit("+".join(list(config.keys())))
x = 0

print("Streaming submissions...")
for post in sub.stream.submissions():

    try:
        if x < 100:
            x += 1
            time.sleep(0.1)
            continue
        else:
            print()
        if post.is_self:
            continue
        sub_name = post.subreddit.display_name
        if sub_name not in config.keys():
            print("sub name not in config, fix this!")
            continue

        masto = mastodon.Mastodon(
            access_token=config[sub_name]['mastodon_access_token'],
            api_base_url=os.environ.get("MASTODON_API_BASE_URL")
        )

        print(f"@{masto.me().acct}:login\t\t->\tLogged into Mastodon")
        known_mimetypes = [
            "image/jpeg",
            "image/gif",
            "image/png",
            "image/tiff",
            "image/webp",
            "video/mp4",
            "video/mpeg",
            "video/ogg",
            "video/webm"
        ]
        files = []
        url = post.url
        if post.media:
            if "reddit_video" in post.media.keys():
                url = post.media['reddit_video']['fallback_url']

        try:
            m_media = None
            mime_type = get_content_type(url)
            if mime_type in known_mimetypes:
                fn = get_media(url)
                files.append(fn)
                m_media = masto.media_post(fn, mime_type=mime_type, description=post.title)
                status = masto.status_post(
                    f"{post.title} - {post.author}",
                    media_ids=[m_media],
                    visibility=os.environ.get("MASTODON_VISIBILITY", "unlisted")
                )
                print(f"@{masto.me().acct} posted media to Mastodon: {status.url}")
            else:
                status = masto.status_post(
                    f"{post.title} - {post.author}\n{url}",
                    visibility=os.environ.get("MASTODON_VISIBILITY", "unlisted")
                )
                print(f"@{masto.me().acct} posted to Mastodon: {status.url}")

        except Exception:
            traceback.print_exc()
        finally:
            for file in files:
                try_remove(file)
    except Exception:
        traceback.print_exc()
